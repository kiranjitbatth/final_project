from django.contrib import admin
from NurtureHealthApp.models import (Department, Contact_Us, register_table,add_treatment, cart, Order,Appointment)

# Register your models here.
admin.site.site_header="NURTURE HEALTH"

class Contact_UsAdmin(admin.ModelAdmin):
    fields = ["contact_number","name","subject","message","address"]

    list_display = ["id","name","contact_number","address","subject","message","added_on"]
    search_fields = ["name"]
    list_filter = ["added_on","name"]
    list_editable = ["name"]

class DepartmentAdmin(admin.ModelAdmin):
    list_display = ["id","Dep_name","description","added_on"]

class AppointmentAdmin(admin.ModelAdmin):
    fields=["patient_name","patient_contactnumber","patient_emailid","disease","appointment_date","message"]
    list_display = ["id","patient_name","patient_contactnumber","patient_emailid","disease","appointment_date","message"]


admin.site.register(Contact_Us,Contact_UsAdmin)
admin.site.register(Department,DepartmentAdmin)
admin.site.register(register_table)
admin.site.register(add_treatment)
admin.site.register(cart)
admin.site.register(Order)
admin.site.register(Appointment)


