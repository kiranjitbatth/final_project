# Generated by Django 3.0.4 on 2020-07-03 03:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('NurtureHealthApp', '0013_order'),
    ]

    operations = [
        migrations.CreateModel(
            name='Appointment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('patient_name', models.CharField(max_length=250)),
                ('patient_contactnumber', models.IntegerField(unique=True)),
                ('patient_emailid', models.EmailField(max_length=250)),
                ('disease', models.CharField(max_length=250)),
                ('appointment_date', models.IntegerField()),
                ('message', models.TextField()),
            ],
        ),
    ]
