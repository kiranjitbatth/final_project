    # def student(name,email,roll_no):
    #    print(" Name:{}\n Email: {}\n Roll No: {}".format(name,email,roll_no))


### Positional Arguments
# student("Amandeep",1001,"aman@gmail.com")
# student("Peter","peter@gmail.com",1002)

#   student(name="Nikita",roll_no=1001,email="nk03@gmail.com")
#   student("Sheetal",email="s@gmail.com",roll_no=1001)
#   student("James","s@gmail.com",roll_no=1001)

# def add(x,y):
#     return x+y

# print(add(10,20))
   
#  ##############################################################################

# def multiply(*args):
#     print("Total: ",len(args))
#     rs = 1
#     for i in args:
#         rs *= i
#     print("Result: ",rs)

# multiply()
# multiply(10,29)
# multiply(10,29,4,6,3,4) 



# ################################################################################

def student(name="Test",email="test@gmail.com",roll_no=1):
    print(" Name:{}\n Email: {}\n Roll No: {}".format(name,email,roll_no))

student("Abcd","abcd@gmail.com")
student("Lorem","lorem@gmail.com",1875)

student("Jack")
student()


# def Area(pi,rad=0):
#     return pi*rad**2

# print(Area(3.14))

# ###################################################################################


# def student(**kwargs):
#     # print(kwargs)
#     for k,v in kwargs.items():
#         print("{}: {}".format(k,v))
#     print("-------------------------")

# student(name="Amandeep Kaur")
# student(first_name="Peter",last_name="Parker",roll_no=1)
# student(email="Peter@gmail.com",roll_no=1)